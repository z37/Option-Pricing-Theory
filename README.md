# Option Pricing Theory <Repository Name>
    
## About  <Synopsis>

* Theory notebook. <Abstract>
* Project status:
    
    ![Status](https://img.shields.io/badge/Status-Active-green) <Status>

## Table of contents
1. Basic securities,
2. Call an Put options.
3. Brownian Motion Process. 
4. Stochastic Differential Equation.

### Requirements
<p>
    <img src="Icons/3d.png" width="30" height="30" />
Mathematica v12.
</p>

![Version](https://img.shields.io/badge/Wolfram--Language-V12.-critical) <Version> 

## Contributing  <Reporting issues>
  <!--- If your README is long or you have some specific process or steps you want contributors to follow, consider creating a separate CONTRIBUTING.md file--->
Contribution is welcomed on component (or project if there is no component for that project).
To contribute to <project_name>, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

## License
![Apache License, Version 2.0](https://img.shields.io/hexpm/l/plug?color=orange&label=License&style=flat-square)
